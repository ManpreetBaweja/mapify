import matplotlib
from matplotlib import pyplot as plt
matplotlib.use('TkAgg')
import numpy as np
import time
import serial
import keyboard

ser = serial.Serial('/dev/ttyACM0', baudrate=9600, bytesize=8, parity='N', stopbits =1, timeout=2)
plt.ion()

fig = plt.figure(facecolor='k')
fig.canvas.toolbar.pack_forget() #gets rid of the toolbar in figure
fig.canvas.manager.set_window_title('Map')
mgn = plt.get_current_fig_manager()
#mgn.window.state('zoomed')

ax = fig.add_subplot(1,1,1, polar=True, facecolor= '#006b70')
ax.tick_params(axis='both', colors='w')
r_max = 100 #maximum radius
ax.set_ylim([0.0, r_max])
ax.set_xlim([0.0, np.pi])
ax.set_position([-0.05, -0.05, 1.1, 1.05])
ax.set_rticks(np.linspace(0.0, r_max, 5))
ax.set_thetagrids(np.linspace(0.0, 180, 10))

angles = np.arange(0, 181, 1) # 0 to 180 incrementing by 1
theta = angles * (np.pi / 180)

pols = ax.plot([], linestyle=' ', marker= 'o', markerfacecolor= 'r', markeredgecolor= 'w',
               markeredgewidth= 1.0, markersize= 3.0, alpha= 0.5)

line1 = ax.plot([], color= 'w', linewidth= 3.0)

fig.canvas.draw()
dists =np.ones((len(angles),))
axbackground = fig.canvas.copy_from_bbox(ax.bbox)

# Existing code...

while True:
    try:
        data = ser.readline()
        decoded = data.decode()
        data = (decoded.replace('\r', '')).replace('\n', '')
        vals = [float(ii) for ii in data.split(',') if ii.strip()]
        if len(vals) < 2:
            continue
        angle, dist = vals
        print(angle, dist)

        dists[int(angle)] = dist

        for i, line in enumerate(pols):
            line.set_data(theta, dists)  # Set data for each Line2D object in the pols list

        for i, single_line1 in enumerate(line1):
            single_line1.set_data(np.repeat((angle * (np.pi / 180)), 2),
                                  np.linspace(0.0, r_max, 2))  # Update each Line2D object in line1

        fig.canvas.restore_region(axbackground)
        for line in pols:
            ax.draw_artist(line)

        for single_line1 in line1:
            ax.draw_artist(single_line1)

        fig.canvas.blit(ax.bbox)
        fig.canvas.draw()
        fig.canvas.flush_events()

        '''if keyboard.is_pressed('q'):
            plt.close('all')
            print('Quit application')
            break'''

    except KeyboardInterrupt:
        plt.close('all')
        print('Keyboard Interrupt')
        break

plt.show()

exit()

