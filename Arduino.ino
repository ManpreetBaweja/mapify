

#include <Servo.h>

int trigPin = 9;    // TRIG pin
int echoPin = 8;    // ECHO pin
Servo myServo;      // Create a servo object

float duration_us, distance_cm;
int angle = 0;      // Initial angle for the servo

void setup() {
  // Begin serial port
  Serial.begin(9600);

  // Configure the trigger pin to output mode
  pinMode(trigPin, OUTPUT);
  // Configure the echo pin to input mode
  pinMode(echoPin, INPUT);

  // Attach the servo to pin 10
  myServo.attach(10);
}

void loop() {
  // Move the servo from 0 to 180 degrees at 5-degree intervals
  for (angle = 0; angle <= 180; angle += 5) {
    myServo.write(angle);  // Set the servo angle
    delay(500);  // Wait for the servo to reach the position

    // Generate 10-microsecond pulse to TRIG pin
    digitalWrite(trigPin, HIGH);
    delayMicroseconds(10);
    digitalWrite(trigPin, LOW);

    // Measure duration of pulse from ECHO pin
    duration_us = pulseIn(echoPin, HIGH);

    // Calculate the distance
    distance_cm = 0.017 * duration_us;

    // Print the value to Serial Monitor
    Serial.print(angle);
    Serial.print(",");
    Serial.println(distance_cm);
  }

  // Reset the angle to 0 after reaching 180 degrees
  angle = 0;
  myServo.write(angle); // Set the servo back to 0 degrees
  delay(1000); // Wait for the servo to reach the initial position
}
